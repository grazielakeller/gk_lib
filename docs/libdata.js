/* Index used for searching */
/*
   Fields used:
     url, name, type, filename, authors, routine name, comments, parameters,
     categories, and attributes
*/
title = "Documentation for gk_lib";
subtitle = "Generated by IDLdoc";
libdata = new Array();
libdataItem = 0;



libdata[libdataItem++] = new Array("./gk_dcm.html", "gk_dcm.pro", ".pro file in ./ directory", "gk_dcm.pro", "", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./gk_dcm.html#gk_dcm", "gk_dcm", "routine in gk_dcm.pro", "gk_dcm.pro", "    Graziela Keller, Jul 2018  ", "gk_dcm", "    Loads gk_lib's discrete color maps (dcm).     Library folder and subfolders must be in path     for colormap files to be found.   ", "reverse              Reverses the colormap.   cm         Colormap ID: gk1, gk2, gk3, or gk4.   ", "          -1", "    Let's load a colormap into a plotting routine:    IDL&gt; colormap=gk_dcm('gk1')   IDL&gt; iimage,dist(500),rgb_table=colormap           Now, for gk2:    IDL&gt; colormap=gk_dcm('gk2')   IDL&gt; iimage,dist(500),rgb_table=colormap           and gk3:    IDL&gt; colormap=gk_dcm('gk3')   IDL&gt; iimage,dist(500),rgb_table=colormap        ");
  
  

libdata[libdataItem++] = new Array("./gk_read2d.html", "gk_read2d.pro", ".pro file in ./ directory", "gk_read2d.pro", "", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./gk_read2d.html#gk_read2d", "gk_read2d", "routine in gk_read2d.pro", "gk_read2d.pro", "    Graziela Keller, Jan 2018  ", "gk_read2d", "    Reads in an ascii file in table format and returns a m x n 2D array of doubles, where m is the number of columns and     n is the number of lines in the file, without prior knowledge of those.   ", "skip           Number of lines to be skipped at the beginning of the file.   file           filename.  separator                String used as separator. For example, ' ', or ',', or ';', etc.  Default is white space.   ", "          -1", "    An example:  IDL&gt; im=gk_read2d('mypath/myfile',' ',skip=1)   ");
  
  

libdata[libdataItem++] = new Array("./gk_replicate_vector.html", "gk_replicate_vector.pro", ".pro file in ./ directory", "gk_replicate_vector.pro", "", "", "", "", "          -1", "");
  
  
  libdata[libdataItem++] = new Array("./gk_replicate_vector.html#gk_replicate_vector", "gk_replicate_vector", "routine in gk_replicate_vector.pro", "gk_replicate_vector.pro", "      Graziela Keller   ", "gk_replicate_vector", "gk_replicate_vector Creates an array, from a given m element vector or a given 1 x m array, where the contents of the input are replicated in the ortogonal direction a specified number of times, such that a [1,2,3] vector  replicated  n=2 times would become [[1,2,3],[1,2,3]].   ", "n          Number of times the vector will be  replicated , that is,           the number of elements of the array dimension ortogonal to the           vector.   vector              The vector to be replicated. It can be either a m element vector such as [1,2,3,...]               or a 1 x m array such as [[1],[2],[3],...].   ", "          -1", "      12/19/2016 - Created.        Example 1 - m element vector:  IDL&gt; vector=[1,2,3] IDL&gt; array=gk_replicate_vector(vector,n=3) IDL&gt; help,vector VECTOR          INT       = Array[3] IDL&gt; print,vector       1       2       3 IDL&gt; help,array ARRAY           INT       = Array[3, 3] IDL&gt; print,array       1       2       3       1       2       3       1       2       3        Example 2 - 1 x m array:  IDL&gt; vector=[[1],[2],[3]] IDL&gt; array=gk_replicate_vector([vector,n=3) IDL&gt; help,vector VECTOR          INT       = Array[1, 3] IDL&gt; print,vector       1       2       3 IDL&gt; help,array ARRAY           INT       = Array[3, 3] IDL&gt; print,array       1       1       1       2       2       2       3       3       3         --         A (m x n) or (n x m) array, depending on the dimensions of the iput parameter, vector.   ");
  
  

