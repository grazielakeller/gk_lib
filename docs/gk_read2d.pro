;docformat = 'rst'
;+
; :Description:
;    Reads in an ascii file in table format and returns a m x n 2D array of doubles, where m is the number of columns and 
;    n is the number of lines in the file, without prior knowledge of those.
;
; :Params:
;    file : in, required, type=string
;           filename.
;    separator : in, optional, type=string
;                String used as separator. For example, ' ', or ',', or ';', etc.  Default is white space.
;
; :Keywords:
;    skip : in, optional, type=integer
;           Number of lines to be skipped at the beginning of the file. 
;      
; :Examples:
;    An example::
;
;     IDL> im=gk_read2d('mypath/myfile',' ',skip=1)
;
; :Author: 
;    Graziela Keller, Jan 2018
;-
function gk_read2d,file,separator,skip=skip
 a=strarr(file_lines(file))
 openr,lun,file,/get_lun
 readf,lun,a
 close,lun
 free_lun,lun
 if keyword_set(skip) then a=a[skip:-1]
 if keyword_set(separator) then b=strsplit(a,separator,/extract) else b=strsplit(a,/extract)
 arr2d=double(b.toarray(/transpose))
 return,arr2d
end
