;docformat = 'rst'
;+
; :Description:
;    Loads gk_lib's discrete color maps (dcm).
;    Library folder and subfolders must be in path 
;    for colormap files to be found.
;
; :Params:
;    cm : in, required, type=string
;         Colormap ID: gk1, gk2, gk3, or gk4.
;
; :Keywords:
;    reverse : in, optional, type=boolean
;              Reverses the colormap. 
;      
; :Examples:
;    Let's load a colormap into a plotting routine::
;    
;       IDL> colormap=gk_dcm('gk1')
;       IDL> iimage,dist(500),rgb_table=colormap
;    .. image:: ./gk_colormaps/colormap1_small.png   
;
;    Now, for gk2::
;
;       IDL> colormap=gk_dcm('gk2')
;       IDL> iimage,dist(500),rgb_table=colormap
;    .. image:: ./gk_colormaps/colormap2_small.png 
;
;    and gk3::
;
;       IDL> colormap=gk_dcm('gk3')
;       IDL> iimage,dist(500),rgb_table=colormap
;    .. image:: ./gk_colormaps/colormap3_small.png  
;        
; :Author: 
;    Graziela Keller, Jul 2018
;-
function gk_dcm,cm,reverse=reverse

case cm of
   'gk1': begin
             file='gk_colormap1.sav'
             print, 'Loading colormap gk1'
          end
   'gk2': begin 
             file='gk_colormap2.sav'
             print, 'Loading colormap gk2'
          end
   'gk3': begin 
             file='gk_colormap3.sav'
             print, 'Loading colormap gk3'
          end
   'gk4': begin
             file='gk_colormap4.sav'
             print, 'Loading colormap gk4'
          end
   else: message, 'Error! No such colormap available.'
endcase
restore,file_which(file)
return,cmap
end
