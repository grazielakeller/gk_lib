;docformat = 'rst'
;+
;gk_replicate_vector
;Creates an array, from a given m element vector or a given 1 x m array, where the contents of the input 
;are replicated in the ortogonal direction a specified number of times, such that a 
;[1,2,3] vector "replicated" n=2 times would become [[1,2,3],[1,2,3]].
;
; :Returns:
;      A (m x n) or (n x m) array, depending on the dimensions of the iput parameter, vector.  
;
; :Params: 
;      vector: in, required, type=array
;              The vector to be replicated. It can be either a m element vector such as [1,2,3,...]
;              or a 1 x m array such as [[1],[2],[3],...].
;
; :Keywords:
;      n : in, required, type=integer
;          Number of times the vector will be "replicated", that is, 
;          the number of elements of the array dimension ortogonal to the 
;          vector.
;
; :Examples:
;      Example 1 - m element vector::
;      
;       IDL> vector=[1,2,3] 
;       IDL> array=gk_replicate_vector(vector,n=3)  
;       IDL> help,vector
;       VECTOR          INT       = Array[3]
;       IDL> print,vector
;             1       2       3
;       IDL> help,array
;       ARRAY           INT       = Array[3, 3]
;       IDL> print,array
;             1       2       3
;             1       2       3
;             1       2       3
;
;      Example 2 - 1 x m array::
;      
;       IDL> vector=[[1],[2],[3]]
;       IDL> array=gk_replicate_vector([vector,n=3)
;       IDL> help,vector
;       VECTOR          INT       = Array[1, 3]
;       IDL> print,vector
;             1
;             2
;             3
;       IDL> help,array
;       ARRAY           INT       = Array[3, 3]
;       IDL> print,array
;             1       1       1
;             2       2       2
;             3       3       3
;
; :Uses:
;      --
;
; :Author: 
;      Graziela Keller
;
; :History:
;      12/19/2016 - Created.
;-
function gk_replicate_vector,vector,n=n

l=list(vector)
for i=0,n-2 do l.add,vector

if size(vector,/n_dimensions) eq 2 then array=l.ToArray(dimension=1)
if size(vector,/n_dimensions) eq 1 then array=transpose(l.ToArray())

return,array

end

